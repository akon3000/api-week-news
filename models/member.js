const Model = require('./Model')

class Member extends Model {
  me(id) {
    return this.db.query('SELECT email, name FROM `member` WHERE id = ?;', [id])
  }

  login(email, password) {
    return this.db.query('SELECT * FROM `member` WHERE `email` = ? AND `password` = ?;', [email, password])
  }

  admin(id) {
    return this.db.query('SELECT email FROM `member` WHERE id = ? AND role = ?;', [id, 'admin'])
  }
}

module.exports = Member