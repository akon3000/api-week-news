const Model = require('./Model')

class News extends Model {
  findNewsMaxCount(query) {
    const { start_date, end_date } = query

    return this.db.query(`
      SELECT COUNT(id) AS max_count
      FROM \`news\`
      ${(start_date && end_date) ? `
        WHERE create_date BETWEEN '${start_date}' AND '${end_date}'
      ` : ''}
    `)
  }

  findNewsList(query = {}) {
    const {
      offset = 0,
      limit = 20,
      order_by = 'desc',
      start_date,
      end_date
    } = query

    return this.db.query(`
      SELECT id, title, short_description, create_date, cover_image, create_by
      FROM \`news\`
      ${(start_date && end_date) ? `
        WHERE create_date BETWEEN '${start_date}' AND '${end_date}'
      ` : ''}
      ORDER BY create_date ${order_by}
      LIMIT ?
      OFFSET ?;
    `, [parseInt(limit), parseInt(offset)])
  }

  findNewsById(id) {
    return this.db.query('SELECT * FROM `news` WHERE id = ?;', [id])
  }

  createNews(data = {}, creator) {
    const { title, short_description, long_description, cover_image } = data

    return this.db.query(`
      INSERT INTO \`news\`
        (title, short_description, long_description, create_date, cover_image, create_by)
      VALUES
        (?, ?, ?, NOW(), ?, ?);
    `, [title, short_description, long_description, cover_image, creator])
  }

  editNews(id, data = {}) {
    const { title, short_description, long_description, cover_image } = data

    return this.db.query(`
      UPDATE \`news\` SET
        title = ?,
        short_description = ?,
        long_description = ?,
        cover_image = ?
      WHERE id = ?;
    `, [title, short_description, long_description, cover_image, id])
  }

  deleteNews(id) {
    return this.db.query('DELETE FROM `news` WHERE id = ?', [id])
  }
}

module.exports = News