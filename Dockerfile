# pull a node image from docker hub
FROM node:latest

# set the working dir to /app
WORKDIR /app

# copy everything to container
COPY . .

# install node package module
RUN npm install

# start server inside container
CMD ["npm", "run", "start"]