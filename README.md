# api-week-news
---

This project is example RESTFUL API for interview with company **Bitazza**.
I use NodeJS, Database MySQL and Docker for development this project.
Before installing, you should download and install [NodeJS](https://nodejs.org/en/), [Docker](https://www.docker.com/).

# Installation
---

Clone repository to your computer.

```
git clone git@gitlab.com/akon3000/api-week-news.git
```

# Start project
---

Go to api-week-news directory by terminal.

```
cd api-week-news
```

Run this project by Docker compose

```
- docker-compose up --build
```

# API document
---

I export postman json file for API document. use `week-news.postman_collection.json` and import it to your postman application.