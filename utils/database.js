const mysql = require('mysql')

class Database {

  constructor(config) {
    this.pool = mysql.createPool(config)
  }

  query(sql, args) {
    return new Promise((resolve, reject) => {
      this.pool.query(sql, args, (err, rows) => {
        if (err) {
          return reject(err)
        }
        
        resolve(rows)
      })
    })
  }

  close() {
    return new Promise((resolve, reject) => {
      this.pool.end(err => {
        if (err) {
          return reject(err)
        }
  
        resolve()
      })
    })
  }

}

module.exports = Database