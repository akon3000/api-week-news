const jwt = require('jsonwebtoken')
const auth = require('../middleware/auth')
const Member = require('../models/Member')

module.exports = (app, db) => {

  const memberModel = new Member(db)

  app.post('/login', async (req, res) => {
    const { email, password } = req.body
    try {
      const result = await memberModel.login(email, password)
      
      if (result.length === 0) {
        /**
         * if user not found from database
         */
        return res.send({
          status: false,
          message: 'Not found account'
        })
      }

      // get user from result
      const [user] = result

      // sign token
      const token = jwt.sign({ week_news_member: user.id }, process.env.TOKEN_SECRET, { expiresIn: process.env.TOKEN_LIFE })
      
      // response token to client
      return res.send({ status: true, data: token })

    } catch (err) {
      /**
       * code have error
       * return an error code 500
       */
      res.status(500).send({ status: false, message: 'Internal server error!!' })
    }
  })

  app.get('/me', auth.verifyToken, async (req, res) => {
    const { token } = req
    const decode = jwt.decode(token)

    try {
      const result = await memberModel.me(decode.week_news_member)

      if (result.length === 0) {
        /**
         * if user not found from database
         */
        return res.send({
          status: false,
          message: 'Not found user'
        })
      }

      // get user from result
      const [user] = result 

      // response token to client
      return res.send({ status: true, data: user })

    } catch (err) {
      /**
       * code have error
       * return an error code 500
       */
      res.status(500).send({ status: false, message: 'Internal server error!!' })
    }
  })

  app.get('/me/admin', auth.verifyToken, async (req, res) => {
    const { token } = req
    const decode = jwt.decode(token)

    try {
      const result = await memberModel.admin(decode.week_news_member)

      if (result.length === 0) {
        /**
         * if user not found from database
         */
        return res.send({
          status: false,
          message: 'Permission denied'
        })
      }

      /**
       * User have admin permission
       */
      return res.send({
        status: true,
        message: 'Permission allowed'
      })

    } catch (err) {
      /**
       * code have error
       * return an error code 500
       */
      res.status(500).send({ status: false, message: 'Internal server error!!' })
    }
  })

}