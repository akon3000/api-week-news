const jwt = require('jsonwebtoken')
const auth = require('../middleware/auth')
const Member = require('../models/Member')
const News = require('../models/News')

module.exports = (app, db) => {
  const memberModel = new Member(db)
  const newsModel = new News(db)

  app.get('/news', async (req, res) => {
    const { query } = req

    try {
      const [news, max_count] = await Promise.all([
        newsModel.findNewsList(query),
        newsModel.findNewsMaxCount(query)
      ])

      return res.send({
        status: true,
        data: news,
        max_count: max_count[0].max_count
      })

    } catch (err) {
      /**
       * code have error
       * return an error code 500
       */
      return res.status(500).send({
        status: false,
        message: 'Internal server error!!'
      })
    }
  })

  app.get('/news/:id', async (req, res) => {
    const { params: { id } } = req

    try {
      const [news] = await newsModel.findNewsById(id)

      if (!news) {
        /**
         * case not found news
         */
        return res.status(404).send({
          status: false,
          message: 'Page Not found'
        })
      }

      return res.send({
        status: true,
        data: news
      })

    } catch (err) {
      /**
       * code have error
       * return an error code 500
       */
      return res.status(500).send({
        status: false,
        message: 'Internal server error!!'
      })
    }
  })

  app.post('/news/create', auth.verifyToken, async (req, res) => {
    const { token, body } = req
    const decode = jwt.decode(token)

    try {
      const [user] = await memberModel.admin(decode.week_news_member)

      if (!user) {
        /**
         * if user not have admin permission
         * return an error code 401
         */
        return res.status(401).send({
          status: false,
          message: 'Unauthorized.'
        })
      }

      await newsModel.createNews(body, user.email)

      return res.send({
        status: true,
        message: 'News is created.'
      })

    } catch (err) {
      /**
       * code have error
       * return an error code 500
       */
      return res.status(500).send({
        status: false,
        message: 'Internal server error!!'
      })
    }
  })

  app.put('/news/edit/:id', auth.verifyToken, async (req, res) => {
    const { token, body, params: { id } } = req
    const decode = jwt.decode(token)

    try {
      const [user] = await memberModel.admin(decode.week_news_member)

      if (!user) {
        /**
         * if user not have admin permission
         * return an error code 401
         */
        return res.status(401).send({
          status: false,
          message: 'Unauthorized.'
        })
      }

      await newsModel.editNews(id, body)

      return res.send({
        status: true,
        message: 'News is updated.'
      })

    } catch (err) {
      /**
       * code have error
       * return an error code 500
       */
      return res.status(500).send({ 
        status: false,
        message: 'Internal server error!!'
      })
    }
  })

  app.delete('/news/delete/:id', auth.verifyToken, async (req, res) => {
    const { token, params: { id } } = req
    const decode = jwt.decode(token)

    try {
      const [user] = await memberModel.admin(decode.week_news_member)

      if (!user) {
        /**
         * if user not have admin permission
         * return an error code 401
         */
        return res.status(401).send({
          status: false,
          message: 'Unauthorized.'
        })
      }

      await newsModel.deleteNews(id)

      return res.send({
        status: true,
        message: 'News is deleted.'
      })

    } catch (err) {
      /**
       * code have error
       * return an error code 500
       */
      return res.status(500).send({ 
        status: false,
        message: 'Internal server error!!'
      })
    }
  })
}