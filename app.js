const app = require('express')()
const cors = require('cors')
const bodyParser = require('body-parser')
const Database = require('./utils/Database')

if (!process.env.PORT) {
  require('dotenv').config({ path: '.env' })
}

const db = new Database({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
  connectionLimit: 10
})

app.use(bodyParser.urlencoded({ extended: false })) // create application/x-www-form-urlencoded parser
app.use(bodyParser.json()) // middleware parse request body-data
app.use(cors()) // middleware setup request header

require('./routes/member')(app, db)
require('./routes/news')(app, db)

app.listen(process.env.PORT, () => console.log(`api running on port ${process.env.PORT}`))