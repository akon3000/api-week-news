const jwt = require('jsonwebtoken')

exports.verifyToken = (req, res, next) => {
  const auth = req.headers['authorization']

  if (!auth) {
    /**
     * if there is no token
     * return an error code 403
     */
    return res.status(403).send({
      status: false,
      message: 'No token provided.'
    })
  }

  const [, token] = auth.split(' ')

  if (!token) {
    /**
     * if token not have bearer pattern
     * return an error code 415
     */
    return res.status(415).send({
      status: false,
      message: 'Invalid token.'
    })
  }

  try {
    jwt.verify(token, process.env.TOKEN_SECRET) // verify token
    req.token = token // set token to request
    return next()
  } catch (err) {
    
    if (err.name === 'TokenExpiredError') {
      /**
       * if jwt verify token is expired
       * return an error code 415
       */
      return res.status(415).send({
        status: false,
        message: 'Token is expired'
      })
    }

    /**
     * if jwt verify token is failed
     * return an error code 401
     */
    return res.status(401).send({
      status: false,
      message: 'Unauthorized.'
    })
  }

}