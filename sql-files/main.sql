-- USE DATABASE --

USE weeknews;

-- CREATE TABLE MEMBER --

CREATE TABLE IF NOT EXISTS `member` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(100) UNIQUE NOT NULL,
  `password` VARCHAR(20) NOT NULL,
  `name` VARCHAR(20) NOT NULL,
  `role` ENUM('admin', 'user') NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=INNODB;

-- INSERT ADMIN ACCOUNT --

INSERT INTO `member` (email, password, name, role) VALUES ('admin@weeknews.com', 'weeknews', 'Admin Week-News', 'admin');

-- CREATE TABLE NEWS --

CREATE TABLE IF NOT EXISTS `news` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `short_description` VARCHAR(250),
  `long_description` TEXT,
  `create_date` DATETIME,
  `cover_image` TEXT,
  `create_by` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`create_by`) REFERENCES `member` (`email`)
) ENGINE=INNODB;